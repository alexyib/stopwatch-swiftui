//
//  ContentView.swift
//  Stopwath-SwiftUI
//
//  Created by Alexy Ibrahim on 1/18/23.
//

import SwiftUI
import Combine

struct ContentView: View {
    @ObservedObject private var viewModel = ContentViewModel()
    @State private var timeValue = ""
    private var observers: [AnyCancellable] = []
    
    var timerUI: some View {
        VStack {
            Spacer()
            
            Text(self.timeValue)
                .font(.system(size: 22, weight: .regular, design: .monospaced))
                .onReceive(self.viewModel.timeUpdated) { value in
                    self.timeValue = self.viewModel.displayTime(value)
                }
            
            Spacer().frame(maxHeight: 16)
                .background(Color.pink)
            
            Button(action: {
                switch self.viewModel.timerState {
                case .started:
                    self.viewModel.stopTimer()
                case .stopped:
                    self.viewModel.startTimer()
                case .none:
                    break
                }
            }) {
                ZStack {
                    switch self.viewModel.timerState {
                    case .started:
                        Circle()
                            .fill(Color.red)

                        Text("Stop").foregroundColor(.white)
                            .font(.system(size: 30, weight: .bold, design: .monospaced))
                    case .stopped:
                        Circle()
                            .fill(Color.blue)

                        Text("Start").foregroundColor(.white)
                            .font(.system(size: 30, weight: .bold, design: .monospaced))
                    case .none:
                        Text("")
                    }
                }
            }
            .frame(maxWidth: .infinity)
            .padding(.horizontal, 100)
            .foregroundColor(.accentColor)
            
            Spacer()
        }
        .padding()
    }
    
    var body: some View {
        NavigationView {
            timerUI
            .navigationTitle(Text("Stopwatch"))
        }
    }
}


struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
