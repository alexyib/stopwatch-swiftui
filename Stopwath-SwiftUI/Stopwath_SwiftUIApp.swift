//
//  Stopwath_SwiftUIApp.swift
//  Stopwath-SwiftUI
//
//  Created by Alexy Ibrahim on 1/18/23.
//

import SwiftUI

@main
struct Stopwath_SwiftUIApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
