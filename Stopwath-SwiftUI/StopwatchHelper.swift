//
//  StopwatchHelper.swift
//  StopWatch
//
//  Created by Alexy Ibrahim on 1/25/23.
//


import Foundation
import Combine

class StopwatchHelper: ObservableObject {
    enum TimerSate {
        case started
        case stopped
    }
    
    @Published private(set) var timerState: TimerSate = .stopped
    private var timer: AnyCancellable?
    private var startDate: Date?
    private(set) var elapsedTime: TimeInterval = .zero
    private let interval: TimeInterval!
    private(set) var timeUpdated: PassthroughSubject! = PassthroughSubject<Double, Never>()
    
    init(interval: TimeInterval) {
        self.interval = interval
    }
    
    func start() {
        if self.interval > 0 {
            self.timerState = .started
            DispatchQueue.main.async {
                self.startDate = Date()
                self.timer = Timer.publish(every: self.interval, tolerance: nil, on: .main, in: .common, options: nil).autoconnect().sink(receiveValue: { [weak self] value in
                    guard let self = self else { return }
                    guard let startDate = self.startDate else { return }
                    self.elapsedTime = Date().timeIntervalSince(startDate)
                    var time = Double(self.elapsedTime)
                    time = (time * 100).rounded() / 100
                    self.timeUpdated.send(time)
                })
            }
        }
    }
    
    func stop() {
        self.timer?.cancel()
        self.timer = nil
        self.timerState = .stopped
        self.startDate = nil
        self.elapsedTime = .zero
    }
}
