//
//  ContentViewModel.swift
//  Stopwath-SwiftUI
//
//  Created by Alexy Ibrahim on 1/18/23.
//

import Foundation
import Combine
import SwiftUI

class ContentViewModel: ObservableObject {
    private var stopwatch: StopwatchHelper = StopwatchHelper(interval: 0.01)
    private var timerStateObservable: AnyCancellable?
    
    init() {
        self.timerStateObservable = stopwatch.$timerState.sink { value in
            self.timerState = value
        }
    }
    
    private var hourInSeconds: Int = (1 * 60 * 60)
    
    @Published internal var timerState: StopwatchHelper.TimerSate!
    
    internal var timeUpdated: PassthroughSubject<Double, Never> {
        self.stopwatch.timeUpdated
    }
    
    internal func startTimer() {
        self.stopwatch.start()
    }
    
    internal func stopTimer() {
        self.stopwatch.stop()
    }
    
    internal func displayTime(_ time: Double) -> String {
        let timeInt = Int(time)
        let mm: Int = (timeInt % self.hourInSeconds) / 60
        let ss: Int = (timeInt % self.hourInSeconds) % 60
        let hh: Int = Int(time.truncatingRemainder(dividingBy: 1) * 100)
        return String(format: "%.2d:%.2d:%.2d", mm, ss, hh)
    }
}
